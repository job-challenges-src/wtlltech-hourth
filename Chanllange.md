# Desafio Hourth | Back-End

# **Data máxima de entrega**

Leia atentamente todo o teste antes de iniciá-lo, afim de evitar erros.

Você terá **até 3h30min** para finalizar este teste a partir do momento em que recebê-lo. Caso não consiga finalizar o teste por completo, envie o projeto assim mesmo. Testes enviados após o tempo estipulado não serão aceitos!

# **Objetivo:**

Você dever criar uma API  (`api_name = struct_data`) que retornará através de um método GET, um JSON nessa estrutura:

```json
{
    "product_url__image":"https://cdn.shopify.com/s/files/1/1602/2781/products/AllSwatches_Render-188588.jpg?v=1605714948",
    "product_url":"https://baebrow.com/products/baebrow-instant-tint-graphite",
    "product_url__created_at":"2019-06-26",
    "total_sales":"118",
    "2020-11-14":"21",
    "2020-11-13":"32",
    "2020-11-12":"25",
    "2020-11-11":"40"
}
```

# Cenário:

Essa API (`api_name = raw_data`) consumirá os dados do seguinte endpoint: [https://mc3nt37jj5.execute-api.sa-east-1.amazonaws.com/default/hourth_desafio](https://mc3nt37jj5.execute-api.sa-east-1.amazonaws.com/default/hourth_desafio)  

Abaixo temos o exemplo de 1 item do endpoint acima:

```json
   {
      "product_url__image":"https://cdn.shopify.com/s/files/1/1602/2781/products/AllSwatches_Render-188588.jpg?v=1605714948",
      "product_url":"https://baebrow.com/products/baebrow-instant-tint-graphite",
      "product_url__created_at":"2019-06-26",
      "consult_date":"2021-02-11",
      "vendas_no_dia":6
   }
```

# O que fazer?

- Agrupar as vendas dos mesmos produtos (determinados a partir da chave `product_url`*)* e:
    - Tornar os valores da `consult_date` em chaves dentro do json
    - Somar os valores da coluna `vendas_no_dia`(de cada um dos dias) e atribuir esse valor somado à chave `total_sales`
- Exemplo: (Entrada) **(Dados que vêm da API `raw_data`)**

```json
[
	{
    "product_url__image":"https://cdn.shopify.com/s/files/1/1602/2781/products/AllSwatches_Render-188588.jpg?v=1605714948",
    "product_url":"https://baebrow.com/products/baebrow-instant-tint-graphite",
    "product_url__created_at":"2019-06-26",
    "consult_date":"2021-02-11",
    "vendas_no_dia":6
	},
	{
    "product_url__image": "https://cdn.shopify.com/s/files/1/1602/2781/products/AllSwatches_Render-188588.jpg?v=1605714948",
    "product_url": "https://baebrow.com/products/baebrow-instant-tint-graphite",
    "product_url__created_at": "2019-06-26",
    "consult_date": "2021-02-13",
    "vendas_no_dia":12
	}
]
```

- Exemplo: (Saída) (**Como deve ficar? API `struct_data`)**

```json
{
    "product_url__image":"https://cdn.shopify.com/s/files/1/1602/2781/products/AllSwatches_Render-188588.jpg?v=1605714948",
    "product_url":"https://baebrow.com/products/baebrow-instant-tint-graphite",
    "product_url__created_at":"2019-06-26",
    "total_sales":"18",
    "2020-11-13":"12",
		"2020-11-12":"0"
    "2020-11-11":"6"
}
```

# **Regra de negócio:**

- Ao acionar o método GET criado por você, este receberá  dois valores como parâmetro: *init_date* e *finish_date*. Estes servirão para determinar o range de filtro do JSON a ser obtido a partir do endpoint **`raw_data`**.
    - O range de data vai filtrar os itens do json em que a chave `consult_date` seja **maior ou igual** a *init_date* e **menor ou igual** a *finish_date*.
    - Caso os parâmetros de *init_date* e *finish_date* **NÃO** sejam fornecidos ao se acionar o método GET, o filtro **NÃO** deverá ser aplicado
    - Caso entre a data inicial (*init_date*) e a data final (*finish_date*) exista um ou mais dias **SEM** vendas **OU** **SEM** representação no JSON de entrada **`struct_data`** , estes devem constar mesmo assim no JSON de saída, apresentando aquele dia com valor (número de vendas) igual a 0.
        
        No Exemplo: (Saída) **`struct_data`** acima. Supondo um filtro com data inicial (*init_date*) em `2020-11-11` e data final (*finish_date*) em `2020-11-13`, em que NÃO houve registro de vendas no dia `2020-11-12`, mesmo assim este é apresentado, mas com valor igual a 0.
        

# **Tecnologias para serem utilizadas:**

- Framework -> Django

# **Como entregar**

Faça o upload do código ou do (.zip) em alguma plataforma de sua preferência. Ex: github, google drive, dropbox, skydrive. **GARANTA QUE O LINK ESTÁ PÚBLICO** e envie o link do projeto para `thiago@hourth.com` e `rh@wtlltech.com` em um **ÚNICO EMAIL** com o **assunto** → `Desafio Hourth - (seu nome)` 

# Diferencial

- Construir uma tabela HTML, conforme o exemplo abaixo (**NÃO HÁ NECESSIDADE DE CUSTOMIZAÇÕES OU CSS**) para exibir os dados agrupados do Exemplo: Saída **`struct_data`**
    
    [Tabela exemplo](https://www.notion.so/Tabela-exemplo-b1f377b7b4c2407b9b394b8eb38a4d62)
    
- Usar o próprio templates do Django. Não precisa utilizar React/Vue/etc...