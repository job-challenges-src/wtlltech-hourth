up:
	sudo docker-compose up

test:
	sudo docker-compose run app \
	pytest -x -s -v -vv --durations=0 --cache-clear
