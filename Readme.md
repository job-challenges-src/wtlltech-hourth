# Desafio Hourth

## Getting started

Para rodar o projeto você precisa ter instalado o `docker` e `docker-compose`.

Para iniciar o projeto você precisa executar somente um simples comando.

```bash
sudo docker-compose up
```
ou caso queira utilizar o alias do projeto.

```bash
make up
```

Para rodar os testes unitários:

```bash
sudo docker-compose run app pytest -x -s -v
```
ou caso queira utilizar o alias do projeto.

```bash
make test
```


###  Roadmap

- [✔️] Endpoint [struct_data](http://localhost:8000/api/struct_data).


### Link da api
`http://localhost:8000/api/struct_data`