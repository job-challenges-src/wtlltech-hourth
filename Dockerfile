FROM python:3.10
# Variáveis de ambiente
ENV PYTHONUNBUFFERED 1

RUN mkdir /app
WORKDIR /app

COPY requirements.txt /app/
COPY dev-requirements.txt /app/
RUN pip install -r /app/dev-requirements.txt

ADD . /app/

EXPOSE 8000