from rest_framework.exceptions import APIException

class ProductServiceUnavailable(APIException):
    status_code = 503
    default_detail = 'Serviço temporariamente indisponível, tente novamente mais tarde.'
    default_code = 'service_unavailable'