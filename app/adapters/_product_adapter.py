from typing import Any, Dict, List

from app.adapters.interfaces import ProductAdapterInterface
from app.dtos import ProductDTO


class ProductAdapter(ProductAdapterInterface):

    @classmethod
    def from_dict(self, products: List[Dict[str, Any]]) -> List[ProductDTO]:
        return [ProductDTO(**product) for product in products]

    @classmethod
    def to_dict(self, products: List[ProductDTO]) -> List[Dict[str, Any]]:
        return [ product.__dict__ for product in products ]