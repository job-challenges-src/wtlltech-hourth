from typing import Any, Dict, List
from collections import OrderedDict
 
from app.adapters.interfaces import ProductStrictDataAdapterInterface
from app.dtos import ProductDTO


class ProductStrictDataAdapter(ProductStrictDataAdapterInterface):

    @classmethod
    def execute(self, products: List[ProductDTO]) -> List[Dict[str, Any]]:
        serialized_data = {}
        for product in products:
            if product.product_url not in serialized_data:
                
                serialized_data[product.product_url] = OrderedDict(
                    product_url= product.product_url,
                    product_url__image= product.product_url__image,
                    product_url__created_at= product.product_url__created_at,
                    total_sales= 0
                )
                
            serialized_data[product.product_url][product.consult_date] = product.vendas_no_dia
            serialized_data[product.product_url]['total_sales'] += int(product.vendas_no_dia)
        
        return [dict(i) for i in serialized_data.values()]