from abc import ABC, abstractclassmethod
from typing import Any, Dict, List

from app.dtos import ProductDTO

class ProductStrictDataAdapterInterface(ABC):

    @abstractclassmethod
    def execute(self, products: List[ProductDTO]) -> List[Dict[str, Any]]:
        ...