from abc import ABC, abstractclassmethod
from typing import Any, Dict, List

from app.dtos import ProductDTO

class ProductAdapterInterface(ABC):

    @abstractclassmethod
    def from_dict(self, products: List[Dict[str, Any]]) -> List[ProductDTO]:
        ...
    
    @abstractclassmethod
    def to_dict(self, products: List[ProductDTO]) -> List[Dict[str, Any]]:
        ...