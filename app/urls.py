from django.urls import path
from app import views

urlpatterns = [
    path('struct_data', views.StructDataAPIView.as_view(), name='strict_data')
]
