from django.test import TestCase, override_settings

from app.adapters import ProductStrictDataAdapter
from app.factories import product_repository_factory

class StructDataAdapterTestCase(TestCase):
    """
        Normalmente faria mais testes
        mas devido ao tempo farei só o básico.
    """

    def setUp(self) -> None:
        self.respository = ProductStrictDataAdapter()
        return super().setUp()

    def test_strict_adapter_success(self):
        """Should be returns expected structure"""
        product_respository = product_repository_factory(test=True)
        expected_data = {
            "product_url__image": "https://cdn.shopify.com/s/files/1/1602/2781/products/AllSwatches_Render-188588.jpg?v=1605714948",
            "product_url": "https://baebrow.com/products/baebrow-instant-tint-graphite",
            "product_url__created_at": "2019-06-26",
            "total_sales": 22,
            "2021-02-11": 6,
            "2021-02-12": 10,
            "2021-02-13":  6,
        }
        products = product_respository.all()
        serializerd_product = self.respository.execute(products)[0]
        assert serializerd_product == expected_data
        assert serializerd_product['total_sales'] == 22

    @override_settings(PRODUCTS_URL='http://fjlasfjlajflkaflkj.com')
    def test_strict_adapter_error(self):
        """Should be returns expected structure"""
        product_respository = product_repository_factory()
        with self.assertRaises(Exception):
            product_respository.all()
        