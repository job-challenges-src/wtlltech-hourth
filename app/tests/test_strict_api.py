from rest_framework.test import APIRequestFactory
from rest_framework.status import HTTP_200_OK, HTTP_503_SERVICE_UNAVAILABLE

from django.test import TestCase, override_settings

from app.views import StructDataAPIView
from app.exceptions import ProductServiceUnavailable


factory = APIRequestFactory()

class StructDataAPITestCase(TestCase):
    """
        Normalmente faria mais testes
        mas devido ao tempo farei só o básico.
    """

    def setUp(self) -> None:
        self.view = StructDataAPIView()
        return super().setUp()

    def test_strict_api_succes(self):
        request = factory.get('')
        response = self.view.get(request)
        assert response.status_code == HTTP_200_OK


    @override_settings(PRODUCTS_URL='http://fjlasfjlajflkaflkj.com')
    def test_strict_api_error(self):
        """Should be returns expected structure"""
        request = factory.get('')
        with self.assertRaises(ProductServiceUnavailable):
            response = self.view.get(request)
            assert response.status_code == HTTP_503_SERVICE_UNAVAILABLE