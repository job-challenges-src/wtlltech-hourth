from typing import Type
from app.adapters import ProductAdapter
from app.repositories import ProductGatewayRepository, ProductInMemoryRepository

from app.repositories.interfaces import ProductRepositoryInterface


def _product_inmemory_repository_factory() -> Type[ProductRepositoryInterface]:
    return ProductInMemoryRepository(ProductAdapter)

def _product_gateway_repository_factory() -> Type[ProductRepositoryInterface]:
    return ProductGatewayRepository(ProductAdapter)

def product_repository_factory(test: bool = False) -> Type[ProductRepositoryInterface]:
    if test:
        return _product_inmemory_repository_factory()
    return _product_gateway_repository_factory()