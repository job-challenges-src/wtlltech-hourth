from dataclasses import dataclass
from datetime import date

@dataclass
class ProductDTO:
    product_url__image: str
    product_url: str
    product_url__created_at: date
    consult_date: date
    vendas_no_dia: int