from rest_framework.views import APIView
from rest_framework.request import Request
from rest_framework.response import Response

from app.adapters import ProductStrictDataAdapter
from app.exceptions import ProductServiceUnavailable
from app.factories import product_repository_factory
from app.repositories.interfaces import ProductRepositoryInterface


class StructDataAPIView(APIView):

    def __init__(self, repository: ProductRepositoryInterface = None, **kwargs) -> None:
        self.repository = repository or product_repository_factory()
        super().__init__(**kwargs)

    def get(self, request: Request) -> Response:
        try:
            data = self.repository.all()
        except Exception:
            raise ProductServiceUnavailable
        serialized_data = ProductStrictDataAdapter.execute(data)
        return Response(serialized_data)