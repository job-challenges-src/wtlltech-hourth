import requests
from typing import List
from rest_framework.status import HTTP_200_OK
from django.conf import settings

from app.dtos import ProductDTO
from app.repositories.interfaces import ProductRepositoryInterface

class ProductGatewayRepository(ProductRepositoryInterface):

    def all(self) -> List[ProductDTO]:
        response: requests.Response = requests.get(settings.PRODUCTS_URL)
        if not response.status_code == HTTP_200_OK:
            raise Exception('Serviço de produtos indisponível.')
        return self._adapter.from_dict(response.json())