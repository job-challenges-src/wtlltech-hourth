from abc import ABC, abstractmethod
from typing import List, Type

from app.adapters.interfaces import ProductAdapterInterface
from app.dtos import ProductDTO


class ProductRepositoryInterface(ABC):

    def __init__(self, adapter: Type[ProductAdapterInterface]) -> None:
        self._adapter = adapter

    @abstractmethod
    def all(self) -> List[ProductDTO]:
        ...
