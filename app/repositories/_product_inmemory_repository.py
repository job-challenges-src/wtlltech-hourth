import json
from typing import List

from app.dtos import ProductDTO
from app.repositories.interfaces import ProductRepositoryInterface
from django.conf import settings

class ProductInMemoryRepository(ProductRepositoryInterface):

    def all(self) -> List[ProductDTO]:
        try:
            with open(f'{settings.BASE_DIR}/app/tests/mocks/products_data.json') as file:
                data = json.loads(file.read())
            return self._adapter.from_dict(data)
        except Exception:
            raise Exception('Serviço de produtos indisponível.')